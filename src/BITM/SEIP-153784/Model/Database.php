<?php


namespace App\Model;

use PDO,PDOException;

class Database
{
    public $DBH;

    public function __construct()
    {

        if(!isset($_SESSION))session_start();

        date_default_timezone_set("UTC");
        date_default_timezone_set("Asia/Dhaka");

        $host = "localhost";
        $dbname = "atomic_project";
        $user = "root";
        $pass = "";

        try{
            $this->DBH = new PDO('mysql:host='.$host.';dbname='.$dbname, $user, $pass);
           // echo "CONGRATULATIONS! Database Established Successfully.<br>";

        }catch (PDOException $error){
            //echo "ERROR! Database Failed to Established, and the ERROR is: ".$error->getMessage()."<br>";
        }
    }
}