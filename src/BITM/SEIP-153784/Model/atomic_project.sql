-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2017 at 01:50 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(1, 'Village', 'K.K.TAHAR', 'Yes'),
(2, 'KOU', 'TUI', 'No'),
(3, 'Apu', 'Rome', 'Yes'),
(4, 'sasd', 'sad', 'No'),
(5, 'Apu', 'TUI', 'No'),
(6, 'sadasd', 'sadad', 'No'),
(7, 'KOU', 'TUI', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `u_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `u_name`, `city`, `soft_delete`) VALUES
(1, 'Russel', 'Chittagong', 'No'),
(2, 'Taimur', 'Khulna', 'No'),
(3, 'Khaimar', 'Rajshahi', 'Yes'),
(4, 'Sabah', 'Sylhet', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `dob`
--

CREATE TABLE `dob` (
  `id` int(11) NOT NULL,
  `u_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `dob` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dob`
--

INSERT INTO `dob` (`id`, `u_name`, `dob`, `soft_delete`) VALUES
(1, 'Noor', '8/3/1988', 'No'),
(2, 'Qawhar', '14/11/1980', 'No'),
(3, 'Nouman', '28/8/1982', 'No'),
(4, 'Zishan', '9/4/2000', 'No'),
(5, 'Tukur', '30/10/2002', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `u_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `u_name`, `email`, `soft_delete`) VALUES
(1, 'Khorshed', 'khorshed@abc.com', 'No'),
(2, 'Tahseen', 'tahseen@xyz.com', 'No'),
(3, 'Jahed', 'jahed@oop.com', 'Yes'),
(4, 'Saad', 'saad@php.com', 'No'),
(5, 'Noor', 'noor@java.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `u_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `u_name`, `gender`, `soft_delete`) VALUES
(1, 'russel', 'Male', 'No'),
(2, 'Imtu', 'Male', 'No'),
(3, 'Fauzia', 'Female', 'Yes'),
(4, 'Taumur', 'Male', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `u_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `hobbies` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `u_name`, `hobbies`, `soft_delete`) VALUES
(2, 'Toha', 'Traveling, Clash of Clans', 'No'),
(3, 'Junaid', 'Traveling, Coding, Clash of Clans', 'No'),
(4, 'Noor', 'Traveling', 'No'),
(5, '', 'Traveling, Coding', 'No'),
(6, '', 'Clash of Clans', 'No'),
(8, '', 'Traveling<br>Coding<br>Clash of Clans<br>Walking', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `u_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `profile` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `u_name`, `profile`, `soft_delete`) VALUES
(1, 'Junaid', '1485906405php.png', 'No'),
(2, 'Tahseen', '1485906445java.png', 'No'),
(3, 'Noor', '1485906459Eyes.png', 'No'),
(4, 'Taimur', '1485906472jquery.png', 'No'),
(5, 'Najal', '1485906492DOWN.png', 'No'),
(6, 'Toha', '1485906499python-logo-master-v3-TM-flattened.png', 'No'),
(7, 'Junaid', '1485906405php.png', 'Yes'),
(8, 'Taimur', '1485906472jquery.png', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `sum_of_organization`
--

CREATE TABLE `sum_of_organization` (
  `id` int(11) NOT NULL,
  `org_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `soft_delete` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sum_of_organization`
--

INSERT INTO `sum_of_organization` (`id`, `org_name`, `summary`, `soft_delete`) VALUES
(1, 'Ispahani ', 'TEA', 'No'),
(2, 'Phillips', 'TubeLight', 'Yes'),
(3, 'KGN', 'Restaurant', 'No'),
(4, 'Dhaba', 'Fast Foods', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dob`
--
ALTER TABLE `dob`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sum_of_organization`
--
ALTER TABLE `sum_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dob`
--
ALTER TABLE `dob`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sum_of_organization`
--
ALTER TABLE `sum_of_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
