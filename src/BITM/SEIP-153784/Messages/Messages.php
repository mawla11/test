<?php


namespace App\Messages;

if(!isset($_SESSION)){session_start();}
class Messages
{

    public static function messages($message=NULL){

        if(is_null($message)){
            $_message = self::getMessages();
            return $_message;
        }else{
            self::setMessages($message);
        }
    }


    public static function setMessages($message){
        $_SESSION['message'] = $message;
    }




    public static function getMessages(){

        if(isset($_SESSION['message'])) $_message = $_SESSION['message'];
        else $_message = "";

        $_SESSION['message'] = "";
        return $_message;
    }
}