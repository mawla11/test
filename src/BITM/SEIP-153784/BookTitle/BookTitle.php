<?php


namespace App\BookTitle;

    use App\Messages\Messages;
    use App\Model\Database as DB;
    use App\Utilities\Utilities;
    use PDO;


    class BookTitle extends DB
{
    private $id;
    private $book_name;
    private $author_name;
    private $soft_delete;

    public function setData($postData=NULL){

        if(array_key_exists('bookName', $postData)){
            $this->book_name = $postData['bookName'];
        }

        if(array_key_exists('id', $postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('authorName', $postData)){
            $this->author_name = $postData['authorName'];
        }

        if(array_key_exists('soft_delete', $postData)){
            $this->soft_delete = $postData['soft_delete'];
        }
    }

    public function store(){

        $dataArray = array($this->book_name,$this->author_name);

        $sql = "INSERT INTO book_title(book_name,author_name) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($dataArray);

        if($result){
            Messages::messages("Successfully data inserted");
        }else{
            Messages::messages("Error! data mot inserted");
        }

        Utilities::redirect("create.php");
    }


    public function index(){

        $sql = "SELECT * FROM book_title WHERE soft_delete = 'No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

        public function view(){

            $sql = "SELECT * FROM book_title WHERE id=".$this->id;
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            return $STH->fetch();
        }

        public function trashed(){

            $sql = "SELECT * FROM book_title WHERE soft_delete = 'Yes'";
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            return $STH->fetchAll();
        }
}

