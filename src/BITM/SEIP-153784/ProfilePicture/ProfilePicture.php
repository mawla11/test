<?php


namespace App\ProfilePicture;


use App\Messages\Messages;
use App\Model\Database as DB;
use App\Utilities\Utilities;
use PDO;

class ProfilePicture extends DB
{

        private $id;
        private $profile;
        private $name;


        public function setData($postData=NULL){

            if(array_key_exists('id',$postData)){
                $this->id = $postData['id'];
            }

            if(array_key_exists('name',$postData)){
                $this->name = $postData['name'];
            }

            if(array_key_exists('profile',$postData)) {
                $this->profile = $postData['profile'];
            }
        }




      public function store(){

            $dataArray = array($this->name,$this->profile);

            $sql = "INSERT INTO profile_picture(u_name,profile) VALUES(?,?)";

            $STH = $this->DBH->prepare($sql);

            $result = $STH->execute($dataArray);

            if($result){
                Messages::messages("Picture Uploaded Successfully");
            }else{
                Messages::messages("Picture Not Uploaded");
            }

            Utilities::redirect('create.php');
        }

            public function index(){
              $sql = "SELECT * FROM profile_picture WHERE soft_delete = 'No'";
              $STH = $this->DBH->query($sql);
              $STH->setFetchMode(PDO::FETCH_OBJ);
              return $STH->fetchAll();
            }

            public function view(){
                $sql = "SELECT * FROM profile_picture WHERE id = ".$this->id;
                $STH = $this->DBH->query($sql);
                $STH->setFetchMode(PDO::FETCH_OBJ);
                return $STH->fetch();
            }

            public function trashed(){
                $sql = "SELECT * FROM profile_picture WHERE soft_delete = 'Yes'";
                $STH = $this->DBH->query($sql);
                $STH->setFetchMode(PDO::FETCH_OBJ);
                return $STH->fetchAll();
            }
}