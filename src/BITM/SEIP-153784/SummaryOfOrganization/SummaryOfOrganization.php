<?php


namespace App\SummaryOfOrganization;

    use App\Messages\Messages;
    use App\Model\Database as DB;
    use App\Utilities\Utilities;
    use PDO;


    class SummaryOfOrganization extends DB
{

    private $id;
    private $organizationName;
    private $summary;
    private $soft_delete;

    public function setData($postData=NULL){

        if(array_key_exists('orgName',$postData)){
            $this->organizationName = $postData['orgName'];
        }

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('summary',$postData)){
            $this->summary = $postData['summary'];
        }

        if(array_key_exists('soft_delete',$postData)){
            $this->soft_delete = $postData['soft_delete'];
        }
    }


    public function store(){

        $dataArray = array($this->organizationName,$this->summary);

        $sql = "INSERT INTO sum_of_organization(org_name,summary) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($dataArray);

        if($result){
            Messages::messages("Data Inserted Successfully");
        }else{
            Messages::messages("ERROR! Data Not Inserted");
        }

        Utilities::redirect('create.php');
    }

    public function index(){

        $sql = "SELECT * FROM sum_of_organization WHERE soft_delete = 'No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view(){

        $sql = "SELECT * FROM sum_of_organization WHERE id =".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function trashed(){

        $sql = "SELECT * FROM sum_of_organization WHERE soft_delete = 'Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}