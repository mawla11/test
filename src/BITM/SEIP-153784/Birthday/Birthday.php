<?php


namespace App\Birthday;

    use App\Model\Database as DB;
    use App\Messages\Messages;
    use App\Utilities\Utilities;
    use PDO;


class Birthday extends DB
{
    private $id;
    private $name;
    private $dob;
    private $soft_delete;

    public function setData($postData=NULL){

        if(array_key_exists('name',$postData)){
            $this->name = $postData['name'];
        }

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];

        }

        if(array_key_exists('dob',$postData)){
            $this->dob = $postData['dob'];

        }

        if(array_key_exists('soft_delete',$postData)){
            $this->soft_delete = $postData['soft_delete'];
        }
    }

    public function store(){
        $dataArray = array($this->name,$this->dob);

        $sql = "INSERT INTO dob(u_name,dob) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($dataArray);

        if($result){
            Messages::messages("Data Inserted Successfully");
        }else{
            Messages::messages("ERROR! Data Not Inserted");
        }

        Utilities::redirect('create.php');
    }

    public function index(){

        $sql = "SELECT * FROM dob WHERE soft_delete = 'No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function view(){

        $sql = "SELECT * FROM dob WHERE id =".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function trashed(){

        $sql = "SELECT * FROM dob WHERE soft_delete = 'Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}