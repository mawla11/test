<?php


namespace App\Gender;

    use App\Model\Database;
    use App\Messages\Messages;
    use App\Utilities\Utilities;
    use PDO;

class Gender extends Database
{
    private $id;
    private $name;
    private $gender;
    private $soft_delete;

    public function setData($postData=NULL){

        if(array_key_exists('name',$postData)){
            $this->name = $postData['name'];
        }

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];

        }

        if(array_key_exists('gender',$postData)){
            $this->gender = $postData['gender'];

        }

        if(array_key_exists('soft_delete',$postData)){
            $this->soft_delete = $postData['soft_delete'];
        }
    }

    public function store(){
        $dataArray = array($this->name,$this->gender);

        $sql = "INSERT INTO gender(u_name,gender) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($dataArray);

        if($result){
            Messages::messages("Data Inserted Successfully");
        }else{
            Messages::messages("ERROR! Data Not Inserted");
        }

        Utilities::redirect('create.php');
    }

    public function index(){

    $sql = "SELECT * FROM gender WHERE soft_delete = 'No'";
    $STH = $this->DBH->query($sql);
    $STH->setFetchMode(PDO::FETCH_OBJ);
    return $STH->fetchAll();
    }

    public function view(){

        $sql = "SELECT * FROM gender WHERE id =".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function trashed(){

        $sql = "SELECT * FROM gender WHERE soft_delete = 'Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}