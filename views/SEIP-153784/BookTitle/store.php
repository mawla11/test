<?php

require_once ("../../../vendor/autoload.php");

    use App\BookTitle\BookTitle;
    use App\Utilities\Utilities;
    use App\Messages\Messages;

    if(isset($_POST['bookName']) && ($_POST['authorName'])) {

        $obj = new BookTitle();

        $obj->setData($_POST);
        $obj->store();
    }else{

        Messages::messages("Please Insert Data");
        Utilities::redirect('create.php');
    }