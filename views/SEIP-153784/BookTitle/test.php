<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>



    <link href="../../../resources/TestStyle.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../../../resources/bootstrap/js/jquery-1.11.1.min.js"></script>
</head>
<body>

    <div class="container">
            <div class="wrapper">
                    <form action="" class="">

                        <table>
                            <tr><th>Book Name </th><td colspan="3"><input type="text" placeholder="Enter Book Name"></td></tr>
                            <tr><th>Author Name </th><td colspan="3"><input type="text" placeholder="Enter Author Name"></td></tr>
                            <tr>
                                <td  colspan="4"><input class="button" type="submit"></td>
                            </tr>
                        </table>

                    </form>
                </div>
    </div>

</body>
</html>