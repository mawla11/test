<?php
    require_once ("../../../vendor/autoload.php");

    use App\BookTitle\BookTitle;

    $objBookTitle = new BookTitle();
    $objBookTitle->setData($_GET);


    $oneData = $objBookTitle->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>View of Book Title</title>

    <link href="../../../resources/style.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../../../resources/bootstrap/js/jquery-1.11.1.min.js"></script>
</head>
<body>
<div class="header">Single Data View of Book Title</div>
    <div class="container2">
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>Book Name</th>
                        <th>Author Name</th>
                    </tr>

                    <?php

                           echo "<tr>
                                    <td>$oneData->id</td>
                                    <td>$oneData->book_name</td>
                                    <td>$oneData->author_name</td>
                                    
                                </tr>";


                    ?>

                </table>
    </div>
<div class="foot"><span>&copy 2017  Md:Golam Sarwer Rakib</span></div>
</body>
</html>