<?php
    require_once ("../../../vendor/autoload.php");

    use App\Email\Email;

    $objEmail = new Email();
    $objEmail->setData($_GET);
    $oneData = $objEmail->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>View of Email</title>

    <link href="../../../resources/style.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../../../resources/bootstrap/js/jquery-1.11.1.min.js"></script>
</head>
<body>
<div class="header">Single Data View of Email</div>
    <div class="container2">
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                    </tr>

                    <?php

                           echo "<tr>
                                <td>$oneData->id</td>
                                <td>$oneData->u_name</td>
                                <td>$oneData->email</td>
                                    
                                </tr>";


                    ?>

                </table>
    </div>
<div class="foot"><span>&copy 2017  Md:Golam Sarwer Rakib</span></div>
</body>
</html>