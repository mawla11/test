<?php
    require_once ("../../../vendor/autoload.php");

    use App\City\City;

    $objCity = new City();
    $allData = $objCity->trashed();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trashed of City</title>


    <link href="../../../resources/style.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../../../resources/bootstrap/js/jquery-1.11.1.min.js"></script>
</head>
<body>
<div class="header">Trashed of City</div>
    <div class="container2">
            <table class="table">
                <tr>
                    <th>Serial</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>

                <?php

                $serial = 1;
                    foreach($allData as $oneData){
                       echo "<tr>
                                <td>$serial</td>
                                <td>$oneData->id</td>
                                <td>$oneData->u_name</td>
                                <td>$oneData->city</td>
                                
                                 <td><a id='buttonView' href='view.php?id=$oneData->id'>VIEW</a></td>
                            </tr>";

                       $serial++;
                    }

                ?>

            </table>
    </div>
<div class="foot"><span>&copy 2017  Md:Golam Sarwer Rakib</span></div>
</body>
</html>