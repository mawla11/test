<?php
require_once ("../../../vendor/autoload.php");

    use App\Messages\Messages;

    $msg = Messages::getMessages();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>City</title>

    <link href="../../../resources/style.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../../../resources/bootstrap/js/jquery-1.11.1.min.js"></script>
</head>
<body>


<div class="header">City Add Form</div>
<div class="container">
    <div class="wrapper">
        <form action="store.php" method="post">

            <table>
                <tr><th>Name </th><td colspan="3"><input type="text" name="name" placeholder="Enter Your Name"></td></tr>
                <tr><th>Select Your City </th><td colspan="3"><select name="city" id="select">
                            <option value="">Select One</option>
                            <option value="Chittagong">Chittagong</option>
                            <option value="Dhaka">Dhaka</option>
                            <option value="Khulna">Khulna</option>
                            <option value="Rajshahi">Rajshahi</option>
                            <option value="Sylhet">Sylhet</option>
                            <option value="Barisal">Barisal</option>
                            <option value="Comilla">Comilla</option>
                            <option value="Cox's Bazar">Cox's Bazar</option>
                        </select></td></tr>
                <tr>
                    <td  colspan="4"><input id="button" type="submit"></td>
                </tr>
            </table>

        </form>
    </div><?php echo "<div id='message'>".$msg."</div>";?>
</div>

<div class="foot"><span>&copy 2017  Md:Golam Sarwer Rakib</span></div>



            <script>

                jQuery(
                    function ($) {
                        $('#message').fadeIn(550);
                        $('#message').fadeOut(550);
                        $('#message').fadeIn(550);
                        $('#message').fadeOut(550);
                        $('#message').fadeIn(550);
                        $('#message').fadeOut(550);
                        $('#message').fadeIn(550);
                        $('#message').fadeOut(550);
                    }
                )
            </script>

</body>
</html>