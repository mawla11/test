<?php
require_once ("../../../vendor/autoload.php");

    use App\Messages\Messages;

            $day = 0;

            $opDay = "";

            for($day = 1; $day <= 31; $day++){
                $opDay .= "<option name='day' value='{$day}'>{$day}</option>";
            }

            $year = 0;

            $opYear = "";

            for($year = date('Y'); $year >= 1953; $year--){
                $opYear .= "<option name='year' value='{$year}'>{$year}</option>>";
            }

    $msg = Messages::getMessages();



?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Birthday</title>

    <link href="../../../resources/style.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <script src="../../../resources/bootstrap/js/jquery-1.11.1.min.js"></script>
</head>
<body>

<div class="header">Birthday Add</div>
    <div class="container">
        <div class="wrapper">

<form action="store.php" method="post">

    <table>
        <tr><th>Name</th><td colspan="3"><input type="text" name="name" placeholder="Enter Your Name"></td></tr>
        <tr><th>Enter Your Birth Date</th></tr>
            <tr><td>
                    <p class="pro"><select name="day" id="select">
                    <option value="">Date</option>
                    <?php echo $opDay;?>
                </select></p>
            </td>
            <td colspan="2"><p class="pro"> <select name="month" id="select">
                    <option value="">Month</option>
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select></p>
            </td>

                <td colspan="2">
                    <p class="pro"><select name="year" id="select" >
                        <option value="">Year</option>
                        <?php echo $opYear;?>
                    </select></p>
                </td>
            </tr>

        <tr>
            <td  colspan="4"><input id="button" type="submit"></td>
        </tr>
    </table>

</form>
        </div><?php echo "<div id='message'>".$msg."</div>";?>
    </div>

<div class="foot"><span>&copy 2017 Md:Golam Sarwer Rakib</span></div>



            <script>

                jQuery(
                    function ($) {
                        $('#message').fadeIn(550);
                        $('#message').fadeOut(550);
                        $('#message').fadeIn(550);
                        $('#message').fadeOut(550);
                        $('#message').fadeIn(550);
                        $('#message').fadeOut(550);
                        $('#message').fadeIn(550);
                        $('#message').fadeOut(550);
                    }
                )
            </script>

</body>
</html>